FROM openjdk:11-jre-slim

MAINTAINER Florian Kimmel <florianmkimmel@gmail.com>

COPY build/libs/race-prediction.jar .

ENTRYPOINT [ "java", "-jar", "./race-prediction.jar" ]
