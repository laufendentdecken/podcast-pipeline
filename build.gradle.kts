import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    kotlin("jvm") version "1.4.10"
    `java-library`
    application
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

val mainClass = "at.laufendentdecken.podcast.PipelineKt"
val log4j = "2.14.0"

repositories {
    mavenCentral()
    maven(url = "https://dl.bintray.com/arrow-kt/arrow-kt/")
    maven(url = "https://www.jitpack.io") {
        name = "jitpack"
    }
    maven (url= "https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(platform("software.amazon.awssdk:bom:2.5.29"))
    implementation("software.amazon.awssdk:s3")

    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:0.7.3")
    implementation("com.github.kittinunf.fuel:fuel:2.2.1")
    implementation("com.google.code.gson:gson:2.8.5")

    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4j")
    implementation("org.apache.logging.log4j:log4j-api:$log4j")
    implementation("org.apache.logging.log4j:log4j-core:$log4j")
}

application {
    mainClassName = mainClass
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "12"
    }
    jar {
        manifest {
            attributes["Main-Class"] = mainClass
            attributes["Implementation-Title"] = "Podcast Pipeline"
        }

        from(configurations.runtimeClasspath.map { elements ->
            elements.files.map {
                if (it.isDirectory) it
                else zipTree(it)
            }
        })

        from(file("src/main/resources/log4j2.xml"))
    }
}

