package at.laufendentdecken.podcast

import com.github.kittinunf.fuel.core.Response
import com.google.gson.Gson
import com.google.gson.JsonObject

fun Response.uuid() =
        data()
                .getAsJsonPrimitive("uuid")
                .asString

fun Response.data(): JsonObject =
        Gson().fromJson(this.body().asString("application/json"), JsonObject::class.java)
                .getAsJsonObject("data")

fun Response.status() =
        data()
                .getAsJsonPrimitive("status_string")
                .asString