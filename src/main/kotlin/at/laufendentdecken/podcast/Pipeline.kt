package at.laufendentdecken.podcast

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import java.io.File
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class Pipeline

val format: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS")
val log: Logger = LogManager.getLogger("standard")

fun main(args: Array<String>) {
    log.info("Upload new podcast episode")
    log.info("-------------------------")

    if (args.isNotEmpty() && File(args[0]).exists()) {
        log.info("File ${File(args[0]).name}")
        run(file = File(args[0]))
    }
}

fun run(file: File) =  file.also { log.info("Start uploading to S3") }
        .uploadToS3(bucket = "laufendentdecken-podcast-backup", region = Region.EU_WEST_3)
        .uploadToS3(bucket = "laufendentdecken-podcast", acl = "public-read", region = Region.EU_CENTRAL_1)
        .copyToLocalBackup().also { log.info("Copied to local backup")}
        .startAuphonicProduction().waitUntilFinished()

private fun File.copyToLocalBackup(): File {
    this.copyTo(File("/Users/florian/Podcast/Backup/${this.name}"))
    return this
}

private fun File.readChapters() = csvReader().readAll(this).drop(1).joinToString("\\n") {
    line -> "${format.format(line[2].toDate())} ${line[1]}"
}

private fun String.toDate(): LocalTime = this.replace(".", ":").split(":").reversed().run {
    LocalTime.of(
            this.toIntOr0(3),
            this.toIntOr0(2),
            this.toIntOr0(1),
            this.toIntOr0(0) * 1000000
    )
}

private fun List<String>.toIntOr0(i: Int) = when {
    this.size >= i + 1 -> this[i].toIntOrNull() ?: 0
    else -> 0
}


private fun File.uploadToS3(bucket: String, acl: String = "", region: Region) : File {
    S3Client
            .builder()
            .region(region)
            .build()
            .putObject(
                    PutObjectRequest
                            .builder()
                            .bucket(bucket)
                            .key(name)
                            .acl(acl)
                            .build(),
                    toPath()
            ).also {
                log.info("S3 Upload finished to $bucket")
            }

    return this
}

private fun File.startAuphonicProduction() : String {
    log.info("Start processing with Auphonic")
    "https://auphonic.com/api/simple/productions.json"
            .httpPost()
            .auphonicAuthentication()
            .apply {
                parameters = listOf(
                        "preset" to "WbQunVJaZFitr3z74XTyxJ",
                        "title" to nameWithoutExtension,
                        "service" to "UWb4qy58MKt5SSRDUeZNrW",
                        "input_file" to name,
                        "title" to nameWithoutExtension,
                        "track" to nameWithoutExtension,
                        "chapters" to File("${absolutePath.replace(".m4a", "")}_markers.csv").readChapters(),
                        "action" to "start"

                )
            }.response().second.run {
        log.info("Auphonic Request Status $statusCode")
        log.info("UUID: ${uuid()}")
        log.info("Status: ${status()}")

        return uuid()
    }
}

private fun String.waitUntilFinished() {
    while(isNotDone()) {
        Thread.sleep(1000)
    }
    log.info("Production is finished")

}

private fun String.isNotDone() : Boolean {
    "https://auphonic.com/api/production/$this.json"
            .httpGet()
            .auphonicAuthentication()
            .response().second.run {
        return status() != "Done"
    }
}

private fun Request.auphonicAuthentication(): Request =
        this.authentication()
        .basic("laufendentdecken", "9B;M28AbTcox8U9UU@i^RJe#H[RTE7iMyo4}/#QxZ2tN2g6Ft/ffWEMyrHVaTp{x")

private fun List<File>.getContent() : File = this[0]


